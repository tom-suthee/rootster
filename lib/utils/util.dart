import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Util {

  static RegExp emailRegExp = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

  static bool isEmail(String email) {
    return emailRegExp.hasMatch(email);
  }

  static bool isXsDown(BuildContext context) {
    return MediaQuery.of(context).size.width < 576;
  }

  static bool isSmDown(BuildContext context) {
    return MediaQuery.of(context).size.width < 768;
  }

  static bool isMdUp(BuildContext context) {
    return MediaQuery.of(context).size.width >= 768;
  }

  static bool isLgUp(BuildContext context) {
    return MediaQuery.of(context).size.width > 992;
  }
}

extension FutureHelper on Future {
  static afterAnimation(Function callback) {
    Future.delayed(Duration(milliseconds: 300), () {
      callback();
    });
  }
}

extension ListHelper<T> on List<T> {
  List<T> prepend(T? element) {
    if (element != null) {
      this.insert(0, element);
    }
    return this;
  }

  List<T> append(T? element) {
    if (element != null) {
      this.add(element);
    }
    return this;
  }

  List<T> appendAll(List<T> elements) {
    this.addAll(elements);
    return this;
  }
}

extension DateTimeHelper on DateTime {
  String mediumDate() {
    return DateFormat('d MMM y').format(this);
  }

  String mediumDateTime() {
    return DateFormat('d MMM y H:m').format(this);
  }
}
