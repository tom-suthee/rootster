import 'package:event_bus/event_bus.dart';

class GlobalEventScreenTap {}

class GlobalEventBus {
  static EventBus _eventBus = EventBus();
  static EventBus get eventBus => _eventBus;
}