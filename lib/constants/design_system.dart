import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

const String BrandFontFamily = 'Sukhumvit';
const double BrandElevation = 6;

const double BrandListTileHeight = 40;

class BrandColor {
  static const Color Primary = Color(0xFFF3C00B);
  static const Color Secondary = Color(0xFFE8F0FE);
  static const Color Black = Color(0xFF1A1A1A);
  static const Color White = Color(0xFFFFFFFF);
  static const Color Grey = Color(0xFFF7F7F7);
  static const Color DarkGrey = Color(0xFF323232);
  
  static const Color Red = Color(0xFFE94787);
  static const Color Green = Color(0xFF60C947);
  static const Color Blue = Color(0xFF4C96F3);
  static const Color LightBlue = Color(0xFFE8F0FE);
  static const Color Yellow = Color(0xFFF3C00B);
  static const Color LightYellow = Color(0xFFFEFDF4);
}

class BrandFontSize {
  static const double Smaller = 11;
  static const double Small = 13;
  static const double Normal = 15;
  static const double Big = 17;
  static const double Bigger = 25;
  static const double Huge = 31;
}

class BrandFontWeight {
  static const FontWeight ExtraLight = FontWeight.w100;
  static const FontWeight Lighter = FontWeight.w200;
  static const FontWeight Light = FontWeight.w300;
  static const FontWeight Normal = FontWeight.w500;
  static const FontWeight Bold = FontWeight.w700;
  static const FontWeight Bolder = FontWeight.w800;
  static const FontWeight ExtraBold = FontWeight.w900;
}

extension BrandStyler on TextStyle {

  static TextStyle get h1 {
    return TextStyle(
      fontFamily: BrandFontFamily,
      fontSize: BrandFontSize.Huge,
      fontWeight: BrandFontWeight.Bold,
      color: BrandColor.Black
    );
  }

  static TextStyle get h2 {
    return TextStyle(
      fontFamily: BrandFontFamily,
      fontSize: BrandFontSize.Bigger,
      fontWeight: BrandFontWeight.Bold,
      color: BrandColor.Black
    );
  }

  static TextStyle get h3 {
    return TextStyle(
      fontFamily: BrandFontFamily,
      fontSize: BrandFontSize.Big,
      fontWeight: BrandFontWeight.Bold,
      color: BrandColor.Black
    );
  }
  
  static TextStyle get header {
    return TextStyle(
      fontFamily: BrandFontFamily,
      fontSize: BrandFontSize.Big,
      fontWeight: BrandFontWeight.Bold,
      color: BrandColor.Black
    );
  }

  static TextStyle get normal {
    return TextStyle(
      fontFamily: BrandFontFamily,
      fontSize: BrandFontSize.Normal,
      fontWeight: BrandFontWeight.Normal,
      color: BrandColor.Black
    );
  }

  static TextStyle get subDetail {
    return TextStyle(
      fontFamily: BrandFontFamily,
      fontSize: BrandFontSize.Small,
      fontWeight: BrandFontWeight.Normal,
      color: BrandColor.Grey
    );
  }

  // Size

  TextStyle get sizeSmaller => this.merge(TextStyle(fontSize: BrandFontSize.Smaller));
  TextStyle get sizeSmall => this.merge(TextStyle(fontSize: BrandFontSize.Small));
  TextStyle get sizeNormal => this.merge(TextStyle(fontSize: BrandFontSize.Normal));
  TextStyle get sizeBig => this.merge(TextStyle(fontSize: BrandFontSize.Big));
  TextStyle get sizeBigger => this.merge(TextStyle(fontSize: BrandFontSize.Bigger));
  TextStyle get sizeHuge => this.merge(TextStyle(fontSize: BrandFontSize.Huge));

  // Weight

  TextStyle get weightExtraLight => this.merge(TextStyle(fontWeight: BrandFontWeight.ExtraLight));
  TextStyle get weightLighter => this.merge(TextStyle(fontWeight: BrandFontWeight.Lighter));
  TextStyle get weightLight => this.merge(TextStyle(fontWeight: BrandFontWeight.Light));
  TextStyle get weightNormal => this.merge(TextStyle(fontWeight: BrandFontWeight.Normal));
  TextStyle get weightBold => this.merge(TextStyle(fontWeight: BrandFontWeight.Bold));
  TextStyle get weightBolder => this.merge(TextStyle(fontWeight: BrandFontWeight.Bolder));
  TextStyle get weightExtraBold => this.merge(TextStyle(fontWeight: BrandFontWeight.ExtraBold));

  // Color

  TextStyle get primary => this.merge(TextStyle(color: BrandColor.Primary));
  TextStyle get secondary => this.merge(TextStyle(color: BrandColor.Secondary));
  TextStyle get white => this.merge(TextStyle(color: BrandColor.White));
  TextStyle get black => this.merge(TextStyle(color: BrandColor.Black));

  TextStyle get grey => this.merge(TextStyle(color: BrandColor.Grey));
  TextStyle get darkGrey => this.merge(TextStyle(color: BrandColor.DarkGrey));

  TextStyle get red => this.merge(TextStyle(color: BrandColor.Red));
  TextStyle get green => this.merge(TextStyle(color: BrandColor.Green));
  TextStyle get blue => this.merge(TextStyle(color: BrandColor.Blue));
  TextStyle get yellow => this.merge(TextStyle(color: BrandColor.Yellow));

  // Decoration

  TextStyle get underline => this.merge(TextStyle(decoration: TextDecoration.underline));
  TextStyle get lineThrough => this.merge(TextStyle(decoration: TextDecoration.lineThrough));
}

// ignore: non_constant_identifier_names
Widget FaIconWithPadding(IconData iconData, {
  Key? key,
  Color? color,
  EdgeInsets? padding,
  double size = 18
}) {
  return Padding(
    key: key,
    padding: padding ?? EdgeInsets.fromLTRB(12, 14, 12, 14),
    child: FaIcon(iconData, size: size, color: color),
  );
}
