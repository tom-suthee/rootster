import 'package:flutter/material.dart';
import 'package:flutter_bootstrap/flutter_bootstrap.dart';
import 'package:rootster/components/rounded-button.dart';
import 'package:rootster/constants/design_system.dart';
import 'package:rootster/utils/util.dart';

class Footer extends StatelessWidget {
  const Footer({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextStyle menuTextStyle = BrandStyler.normal.weightBold.white;

    return Column(
      children: [
        Container(
          padding: EdgeInsets.fromLTRB(0, 40, 0, 40),
          decoration: BoxDecoration(
            color: BrandColor.Black
          ),
          child: BootstrapContainer(
            children: [
              BootstrapRow(
                children: [
                  BootstrapCol(
                    sizes: 'col-12',
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image(
                          image: AssetImage('lib/assets/images/logo-sm.png')
                        )
                      ],
                    )
                  ),
                  BootstrapCol(
                    sizes: 'col-12',
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 15, 0, 20),
                      child: Text(
                        'Online Trusted Industrial Sourcing Platform',
                        textAlign: TextAlign.center,
                        style: BrandStyler.normal.weightBolder.white,
                      ),
                    )
                  ),

                  BootstrapCol(
                    sizes: 'col-12',
                    child: Wrap(
                      alignment: WrapAlignment.center,
                      children: [
                        SizedBox(
                          width: 120,
                          child: RoundedButton(
                            onPressed: () {},
                            title: 'OUR SERVICE',
                            backgroundColor: Colors.transparent,
                            padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                            // insets: EdgeInsets.all(0),
                            textStyle: menuTextStyle.merge(TextStyle().weightExtraBold.yellow),
                          ),
                        ),
                        SizedBox(
                          width: 150,
                          child: RoundedButton(
                            onPressed: () {},
                            title: 'OUR CUSTOMER',
                            backgroundColor: Colors.transparent,
                            padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                            // insets: EdgeInsets.all(0),
                            textStyle: menuTextStyle.merge(TextStyle().weightExtraBold.yellow),
                          ),
                        ),
                        SizedBox(
                          width: 120,
                          child: RoundedButton(
                            onPressed: () {},
                            title: 'PACKAGES',
                            backgroundColor: Colors.transparent,
                            padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                            // insets: EdgeInsets.all(0),
                            textStyle: menuTextStyle.merge(TextStyle().weightExtraBold.yellow),
                          ),
                        ),
                        SizedBox(
                          width: 180,
                          child: RoundedButton(
                            onPressed: () {},
                            title: 'BECOME A SUPPLIER',
                            backgroundColor: Colors.transparent,
                            padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                            // insets: EdgeInsets.all(0),
                            textStyle: menuTextStyle.merge(TextStyle().weightExtraBold.yellow),
                          ),
                        ),
                        SizedBox(
                          width: 120,
                          child: RoundedButton(
                            onPressed: () {},
                            title: 'CONTACT US',
                            backgroundColor: Colors.transparent,
                            padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                            // insets: EdgeInsets.all(0),
                            textStyle: menuTextStyle.merge(TextStyle().weightExtraBold.yellow),
                          ),
                        ),
                      ],
                    ),
                  ),
                  
                ]
              )
            ],
          ),
        ),

        Container(
          decoration: BoxDecoration(
            color: BrandColor.DarkGrey
          ),
          child: BootstrapContainer(
            children: [
              BootstrapRow(
                children: [
                  BootstrapCol(
                    sizes: 'col-12 col-xs-12 col-sm-12 col-md-6',
                    child: Row(
                      mainAxisAlignment: Util.isMdUp(context) ? MainAxisAlignment.start : MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                          child: Text(
                            'Online Trusted Industrial Sourcing Platform',
                            textAlign: TextAlign.center,
                            style: BrandStyler.normal.weightBolder.white,
                          ),
                        ),
                      ],
                    )
                  ),
                  BootstrapCol(
                    sizes: 'col-12 col-xs-12 col-sm-12 col-md-6',
                    child: Row(
                      mainAxisAlignment: Util.isMdUp(context) ? MainAxisAlignment.end : MainAxisAlignment.spaceAround,
                      children: [
                        RoundedButton(
                          onPressed: () {},
                          title: 'Privacy Policy',
                          backgroundColor: Colors.transparent,
                          padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                          // insets: EdgeInsets.all(0),
                          textStyle: BrandStyler.normal.weightBolder.white,
                        ),
                        RoundedButton(
                          onPressed: () {},
                          title: 'Terms & Conditions',
                          backgroundColor: Colors.transparent,
                          padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                          // insets: EdgeInsets.all(0),
                          textStyle: BrandStyler.normal.weightBolder.white,
                        )
                      ],
                    )
                  )
                ]
              )
            ]
          )
        )
      ],
    );
  }
}