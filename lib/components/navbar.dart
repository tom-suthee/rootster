import 'dart:async';
import 'dart:ui';

import 'package:flutter_bootstrap/flutter_bootstrap.dart';
import 'package:flutter_portal/flutter_portal.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rootster/components/rounded-button.dart';
import 'package:rootster/constants/design_system.dart';
import 'package:rootster/utils/global_event_bus.dart';
import 'package:flutter/material.dart';

class NavbarAction {
  final String title;
  final Color color;
  final Color backgroundColor;
  final VoidCallback? onPressed;
  final bool rounded;
  final TextStyle? textStyle;
  final EdgeInsets? insets;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  
  NavbarAction({
    required this.title,
    this.color = BrandColor.White,
    this.backgroundColor = BrandColor.Primary,
    this.onPressed,
    this.rounded = true,
    this.textStyle,
    this.insets,
    this.prefixIcon,
    this.suffixIcon
  });
}

class Navbar extends StatefulWidget with PreferredSizeWidget {
  @override
  final Size preferredSize;
  final bool automaticallyImplyLeading;
  final List<NavbarAction>? actions;

  Navbar(
  {
    Key? key,
    this.automaticallyImplyLeading = true,
    this.actions,
  }
  ) : preferredSize = Size.fromHeight(75.0), super(key: key);

  Navbar.blank({
    Key? key
  }) : 
  automaticallyImplyLeading = true,
  actions = const [],
  preferredSize = Size.fromHeight(75.0),
  super(key: key);

  static Navbar anonymous() {
    final TextStyle menuTextStyle = BrandStyler.normal.weightBold.white;

    return Navbar(
      actions: [
        NavbarAction(title: 'OUR SERVICE', backgroundColor: Colors.transparent, textStyle: menuTextStyle, onPressed: () {}),
        NavbarAction(title: 'OUR CUSTOMER', backgroundColor: Colors.transparent, textStyle: menuTextStyle, onPressed: () {}),
        NavbarAction(title: 'PACKAGES', backgroundColor: Colors.transparent, textStyle: menuTextStyle, onPressed: () {}),
        NavbarAction(title: 'CONTACT US', backgroundColor: Colors.transparent, textStyle: menuTextStyle, onPressed: () {}),
        NavbarAction(title: 'BECOME A SUPPLIER', backgroundColor: Colors.transparent, textStyle: menuTextStyle, onPressed: () {}),
        NavbarAction(
          title: 'LOGIN',
          color: BrandColor.DarkGrey,
          backgroundColor: Colors.yellow,
          rounded: false,
          textStyle: menuTextStyle.merge(TextStyle(color: BrandColor.DarkGrey)),
          insets: EdgeInsets.fromLTRB(20, 8, 20, 10),
          onPressed: () {}
        ),
        NavbarAction(
          title: 'EN',
          backgroundColor: Colors.transparent,
          textStyle: menuTextStyle.merge(TextStyle(color: BrandColor.Yellow)),
          suffixIcon: FaIconWithPadding(
            FontAwesomeIcons.caretDown,
            color: BrandColor.Yellow,
            padding: EdgeInsets.fromLTRB(10, 10, 0, 10)
          ),
          onPressed: () {}
        ),
      ]
    );
  }

  @override
  _NavbarState createState() {
    return _NavbarState(widget: this);
  }
}

class _NavbarState extends State<Navbar> {
  final Navbar widget;
  bool navMenuShow = false;
  late StreamSubscription<GlobalEventScreenTap> gestureEventListener;

  _NavbarState({ required this.widget }) : super();

  @override
  void initState() {
    this.gestureEventListener = GlobalEventBus.eventBus.on<GlobalEventScreenTap>().listen((event) {
      setState(() {
        this.navMenuShow = false;
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    this.gestureEventListener.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: BrandColor.DarkGrey,
      height: this.widget.preferredSize.height,
      child: BootstrapContainer(
        children: [
          BootstrapRow(
            children: [
              BootstrapCol(
                sizes: 'col-12',
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 15, left: 15),
                      width: 120,
                      child: Image(
                        image: AssetImage('lib/assets/images/logo-sm.png')
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 12),
                        child: BootstrapRow(
                          children: [
                            BootstrapCol(
                              sizes: 'col-12',
                              invisibleForSizes: 'xs sm md',
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: List<Widget>.generate(this.widget.actions == null ? 0 : this.widget.actions!.length, (index) {
                                  final NavbarAction action = this.widget.actions![index];
                                  return RoundedButton(
                                    onPressed: action.onPressed,
                                    title: action.title,
                                    foregroundColor: action.color,
                                    backgroundColor: action.backgroundColor,
                                    padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                                    insets: action.insets ?? EdgeInsets.all(0),
                                    rounded: action.rounded,
                                    textStyle: action.textStyle ?? TextStyle().weightBold,
                                    prefixIcon: action.prefixIcon,
                                    suffixIcon: action.suffixIcon,
                                  );
                                }),
                              ),
                            ),
                            BootstrapCol(
                              sizes: 'col-12',
                              invisibleForSizes: 'lg xl xxl xxxl',
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  PortalEntry(
                                    visible: this.navMenuShow,
                                    portalAnchor: Alignment.topRight,
                                    childAnchor: Alignment.bottomRight,
                                    portal: Material(
                                      elevation: 8,
                                      color: BrandColor.DarkGrey,
                                      child: IntrinsicWidth(
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(color: BrandColor.Grey)
                                          ),
                                          padding: const EdgeInsets.all(10.0),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: List<Widget>.generate(this.widget.actions == null ? 0 : this.widget.actions!.length, (index) {
                                              final NavbarAction action = this.widget.actions![index];
                                              return RoundedButton(
                                                onPressed: action.onPressed,
                                                title: action.title,
                                                foregroundColor: action.color,
                                                backgroundColor: action.backgroundColor,
                                                padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                                                insets: action.insets ?? EdgeInsets.all(0),
                                                rounded: action.rounded,
                                                textStyle: action.textStyle ?? TextStyle().weightBold,
                                                prefixIcon: action.prefixIcon,
                                                suffixIcon: action.suffixIcon,
                                              );
                                            }),
                                          ),
                                        ),
                                      ),
                                    ),
                                    child: RoundedButton(
                                      onPressed: () => {
                                        setState(() => {
                                          navMenuShow = !navMenuShow
                                        })
                                      },
                                      title: '',
                                      icon: FaIconWithPadding(
                                        FontAwesomeIcons.bars,
                                        size: 20,
                                        padding: EdgeInsets.only(top: 12, right: 10),
                                        color: BrandColor.White
                                      ),
                                      foregroundColor: BrandColor.White,
                                      backgroundColor: Colors.transparent,
                                      padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                                      rounded: true,
                                      textStyle: TextStyle().weightBold,
                                    ),
                                  )
                                ],
                              )
                            )
                          ]
                        ),
                      ),
                    )
                  ]
                ),
              )
            ]
          )
        ],
      )
    );
  }
}