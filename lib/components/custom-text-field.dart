import 'package:rootster/constants/design_system.dart';
import 'package:rootster/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

typedef BoolVoidFunction = bool Function();
typedef BoolStringFunction = bool Function(String);
typedef NullableStringStringFunction = String? Function(String);
typedef StringStringFunction = String Function(String);
typedef VoidStringFunction = void Function(String);
typedef StringStringBoolFunction = String? Function(String, bool);

class CustomTextFieldController {
  _CustomTextFieldState? state;

  CustomTextFieldController();

  void resetState() {
    if (this.state != null) {
      this.state!.resetState();
    }
  }
}

class CustomTextField extends StatefulWidget {
  final TextEditingController? controller;
  final VoidStringFunction onChanged;
  final VoidStringFunction? onSubmitted;
  final BoolVoidFunction? shouldFocus;
  final StringStringBoolFunction? errorText;
  final String? label;
  final String? hint;
  final String? initialText;
  final double borderRadius;
  final TextInputType textInputType;
  final TextInputAction textInputAction;
  final EdgeInsets? contentPadding;
  final int? maxLength;
  final bool obscureText;
  final int? minLines;
  final int? maxLines;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final CustomTextFieldController? customTextFieldController;

  static const double DefaultBorderRadius = 4;

  const CustomTextField({
    Key? key,
    this.customTextFieldController,
    this.controller,
    required this.onChanged,
    this.onSubmitted,
    this.shouldFocus,
    this.errorText,
    this.label,
    this.hint,
    this.initialText,
    this.borderRadius = CustomTextField.DefaultBorderRadius,
    this.textInputType = TextInputType.text,
    this.textInputAction = TextInputAction.done,
    this.contentPadding,
    this.maxLength,
    this.obscureText = false,
    this.minLines,
    this.maxLines,
    this.prefixIcon,
    this.suffixIcon,
  }) : super(key: key);

  static CustomTextField dropdown({
    Key? key,
    required VoidStringFunction onChanged,
    StringStringFunction? dropdownDisplay,
    StringStringBoolFunction? errorText,
    String? label,
    String? hint,
    double? borderRadius,
    required String? dropdownValue,
    required List<String>? dropdownItems,
  }) {
    return CustomTextField(
      key: key,
      onChanged: onChanged,
      errorText: errorText,
      label: label,
      hint: hint,
      borderRadius: borderRadius ?? CustomTextField.DefaultBorderRadius,
      suffixIcon: FaIconWithPadding(FontAwesomeIcons.caretDown, padding: EdgeInsets.fromLTRB(0, 0, 10, 15)),
    );
  }

  @override
  _CustomTextFieldState createState() {
    final state = _CustomTextFieldState(widget: this);
    if (this.customTextFieldController != null) {
      this.customTextFieldController!.state = state;
    }
    return state;
  }
}

class _CustomTextFieldState extends State<CustomTextField> {
  final CustomTextField widget;
  final FocusNode focusNode = FocusNode();
  late TextEditingController controller;
  bool selfController = false;
  bool hasChanged = false;
  String? errorText;
  String? dropdownValue;
  bool passwordHidden = false;

  _CustomTextFieldState({
    required this.widget
  }) : super() {
    if (this.widget.controller == null) {
      this.controller = TextEditingController();
      this.selfController = true;
    } else {
      this.controller = this.widget.controller!;
    }
  }

  @override
  void initState() {
    this.passwordHidden = this.widget.obscureText;

    this.controller.addListener(() {
      this.hasChanged = true;
      if (this.widget.errorText != null) {
        setState(() {
          this.errorText = this.widget.errorText!(this.controller.text, this.hasChanged);
        });
      }
      this.widget.onChanged(this.controller.text);
    });

    this.focusNode.addListener(() {
      if (this.focusNode.hasFocus) {
        if (this.widget.shouldFocus != null && this.widget.shouldFocus!() == false) {
          FocusScope.of(context).requestFocus(FocusNode());
        }
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    if (this.selfController) {
      this.controller.dispose();
    }

    this.focusNode.dispose();
    super.dispose();
  }

  void resetState() {
    setState(() {
      this.controller.text = '';
      this.hasChanged = false;

      if (this.widget.errorText != null) {
        this.errorText = this.widget.errorText!(this.controller.text, this.hasChanged);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 15),
          child: TextField(
            controller: this.controller,
            focusNode: this.focusNode,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderSide: BorderSide(color: this.errorText == null ? BrandColor.DarkGrey : BrandColor.Red),
                borderRadius: BorderRadius.circular(this.widget.borderRadius)
              ),
              errorText: this.errorText,
              hintText: this.widget.hint,
              filled: true,
              fillColor: BrandColor.White,
              hoverColor: BrandColor.White,
              isDense: true,
              contentPadding: this.widget.contentPadding ?? EdgeInsets.all(8),
              prefixIcon: this.widget.prefixIcon,
              prefixIconConstraints: this.widget.prefixIcon == null ? null : BoxConstraints(
                maxHeight: 8 
              ),
              suffixIcon: this.widget.obscureText == false ? this.widget.suffixIcon : InkWell(
                onTap: () => {
                  setState(() => {
                    this.passwordHidden = !this.passwordHidden
                  })
                },
                child: FaIconWithPadding(
                  this.passwordHidden ? FontAwesomeIcons.eye : FontAwesomeIcons.eyeSlash,
                  padding: EdgeInsets.fromLTRB(10, 10, 15, 0)
                ),
              ), 
              suffixIconConstraints: this.widget.obscureText == false || this.widget.suffixIcon == null ? null : BoxConstraints(
                maxHeight: 20
              ), 
            ),
            obscureText: this.passwordHidden,
            textInputAction: this.widget.textInputAction,
            keyboardType: (((this.widget.minLines ?? 0) > 1 || (this.widget.maxLines ?? 0) > 1)) ?
              TextInputType.multiline :
              this.widget.textInputType,
            maxLength: this.widget.maxLength,
            minLines: this.widget.minLines,
            maxLines: this.widget.obscureText ? 1 : this.widget.maxLines,
            onSubmitted: (value) {
              if (this.widget.textInputAction == TextInputAction.done) {
                FocusScope.of(context).requestFocus(FocusNode());
              }
              
              if (this.widget.onSubmitted != null) {
                this.widget.onSubmitted!(this.controller.text);
              }
            },
          ),
        )
      ].prepend(this.widget.label == null || this.widget.label!.isEmpty ? null :
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(
            this.widget.label!
          ),
        ),
      ),
    );
  }
}
