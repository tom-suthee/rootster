import 'package:flutter_portal/flutter_portal.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rootster/components/custom-text-field.dart';
import 'package:rootster/components/footer.dart';
import 'package:rootster/components/navbar.dart';
import 'package:rootster/components/rounded-button.dart';
import 'package:rootster/constants/design_system.dart';
import 'package:rootster/pages/home_page.dart';
import 'package:rootster/utils/global_event_bus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bootstrap/flutter_bootstrap.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String email = '';
  String password = '';

  final CustomTextFieldController emailTextController = CustomTextFieldController();
  final CustomTextFieldController passwordTextController = CustomTextFieldController();

  Future<void> login() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', 'username');
  }

  Future<void> goToHomePage() async {
    this.emailTextController.resetState();
    this.passwordTextController.resetState();

    FocusScope.of(context).requestFocus(FocusNode());

    String result = await Navigator.push(context, MaterialPageRoute(
      builder: (BuildContext context) => HomePage()
    ));

    this.iAmBack(result);
  }

  void iAmBack(String result) async {
    return await showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Hi There', style: BrandStyler.header),
          content: SingleChildScrollView(
            child: Text('You clicked \'$result\' button', style: BrandStyler.normal),
          ),
          actions: <Widget>[
            RoundedButton(
              title: 'Close',
              foregroundColor: BrandColor.White,
              backgroundColor: BrandColor.DarkGrey,
              rounded: true,
              maxWidth: 80,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            RoundedButton(
              title: 'Do it again!',
              foregroundColor: BrandColor.White,
              backgroundColor: BrandColor.Red,
              rounded: true,
              maxWidth: 80,
              onPressed: () async {
                Navigator.of(context).pop();
                this.goToHomePage();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Portal(
      child: Scaffold(
        appBar: Navbar.anonymous(),
        body: GestureDetector(
          onTap: () => {
            FocusScope.of(context).requestFocus(FocusNode()),
            GlobalEventBus.eventBus.fire(GlobalEventScreenTap())
          },
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  'lib/assets/images/bg-ellipse-1.png'
                ),
                fit: BoxFit.cover,
              )
            ),
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 60, bottom: 60),
                  child: BootstrapContainer(
                    children: [
                      BootstrapRow(
                        children: [
                          BootstrapCol(
                            sizes: 'col-12 col-sm-10 col-md-8',
                            offsets: 'offset-0 offset-sm-1 offset-md-2',
                            child: Column(
                              children: [
                                Text(
                                  'LOG IN CORPORATE USER',
                                  textAlign: TextAlign.center,
                                  style: BrandStyler.h1.weightExtraBold,
                                ),
                                Text(
                                  'Online Trusted Industrial Sourcing Platform',
                                  textAlign: TextAlign.center,
                                  style: BrandStyler.h3.weightNormal,
                                )
                              ],
                            )
                          ),

                          BootstrapCol(
                            sizes: 'col-12 col-sm-10 col-md-8',
                            offsets: 'offset-0 offset-sm-1 offset-md-2',
                            child: Container(
                              margin: EdgeInsets.fromLTRB(0, 40, 0, 20),
                              padding: EdgeInsets.all(15),
                              decoration: BoxDecoration(
                                color: BrandColor.White,
                                boxShadow: [
                                  BoxShadow(
                                    color: BrandColor.Black.withAlpha(30),
                                    offset: Offset(0, 5),
                                    blurRadius: 10,
                                    spreadRadius: 10
                                  )
                                ],
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Text(
                                    'Email',
                                    style: BrandStyler.h3.blue.weightExtraBold,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 10, bottom: 10),
                                    child: CustomTextField(
                                      customTextFieldController: this.emailTextController,
                                      initialText: this.email,
                                      onChanged: (String value) {
                                        setState(() {
                                          this.email = value;
                                        });
                                      },
                                      errorText: (String value, bool hasChanged) {
                                        if (value.isEmpty && hasChanged) {
                                          return 'Email is Required';
                                        }
                                        return null;
                                      },
                                      hint: 'EMAIL',
                                      borderRadius: 8,
                                      contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                                    ),
                                  ),
                                  Text(
                                    'PASSWORD',
                                    style: BrandStyler.h3.blue.weightExtraBold,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 10, bottom: 10),
                                    child: CustomTextField(
                                      customTextFieldController: this.passwordTextController,
                                      initialText: this.password,
                                      onChanged: (String value) {
                                        setState(() {
                                          this.password = value;
                                        });
                                      },
                                      errorText: (String value, bool hasChanged) {
                                        if (value.isEmpty && hasChanged) {
                                          return 'Password is Required';
                                        }
                                        return null;
                                      },
                                      hint: 'PASSWORD',
                                      borderRadius: 8,
                                      obscureText: true,
                                      contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 30),
                                    child: Text(
                                      'Your username is corporate e-mail address or your computer username',
                                      textAlign: TextAlign.center,
                                      style: BrandStyler.normal.weightNormal.black,
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      RoundedButton(
                                        onPressed: this.email.isEmpty || this.password.isEmpty ? null : () {
                                          this.goToHomePage();
                                        },
                                        title: 'LOGIN',
                                        backgroundColor: this.email.isEmpty || this.password.isEmpty ? BrandColor.Grey : BrandColor.Black,
                                        insets: EdgeInsets.fromLTRB(50, 5, 50, 5),
                                        textStyle: BrandStyler.normal.white,
                                      )
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 40, 0, 0),
                                    child: InkWell(
                                      onTap: () {

                                      },
                                      child: Text(
                                        'Forgot password?',
                                        textAlign: TextAlign.center,
                                        style: BrandStyler.normal.weightBold.black,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          )
                        ]
                      )
                    ]
                  ),
                ),

                Footer(),
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {},
          backgroundColor: BrandColor.Blue,
          label: Text(
            'Help',
            style: BrandStyler.normal.weightBold.white,
          ),
          icon: FaIconWithPadding(
            FontAwesomeIcons.questionCircle,
            padding: EdgeInsets.all(0)
          ),
        ),
      ),
    );
  }
}
